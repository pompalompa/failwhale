import twitter

#Replace consumer_key, consumer_secret, acces_token_key, and access_token_secret with own values
api = twitter.Api(consumer_key='consumer_key',
                  consumer_secret='consumer_secret',
                  access_token_key='access_token_key',
                  access_token_secret='access_token_secret')

#Replace userID with user ID
statuses = api.GetUserTimeline(userID)
if len(statuses) == 0:
    print("No tweets to delete")
while len(statuses) != 0:
    for c in statuses:
        print("Destroying tweet with ID " + str(c.id))
        print(c.text + "\n")
        api.DestroyStatus(c.id)
    #Replace userID with user ID
    statuses = api.GetUserTimeline(userID)
